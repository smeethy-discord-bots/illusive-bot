import express from "express";
import mongoose, { Error } from "mongoose";
import SampleEntity, {
  ISampleEntity,
} from "../../core/entities/dbEntities/sampleEntity";

// Dependencies to read the request bodies
const router = express.Router();
var bodyParser = require("body-parser");
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

// POST - Create a new sample entity
router.post("/", async function (req, res) {
  let isEventExisting = await SampleEntity.find({ name: req.body.name });
  if (isEventExisting.length >= 1) {
    return res.status(409).json({
      message: "This entity already exists in the collection.",
    });
  }
  SampleEntity.create(
    {
      name: req.body.name,
      somethingElse: req.body.somethingElse,
    },
    (err: any, entity: ISampleEntity) => {
      if (err)
        return res
          .status(500)
          .send("There was a problem adding the information to the database.");
      res.status(200).send(entity);
    }
  );
});

// GET - Get all the Sample entities
router.get("/", function (req, res) {
  SampleEntity.find({}, (err: Error, allEntities: ISampleEntity[]) => {
    if (err)
      return res.status(500).send("There was a problem finding entities.");
    res.status(200).send(allEntities);
  });
});

export default router;
