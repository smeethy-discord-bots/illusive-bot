import express from 'express';
const baseRouter = express.Router();

// Base controllers - Paths 
baseRouter.get('/', (req, res, next) => {
    res.status(200).json({
        message: "Discord demo backend"
    });
});

export default baseRouter;