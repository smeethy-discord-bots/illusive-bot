import { app } from '../app';
import baseController from './v1/baseController';
import sampleEntityController from './v1/sampleEntityController';

// Register all the controllers
app.use('/', baseController);
app.use('/sampleentity', sampleEntityController);