import { Container } from "inversify";
import { IHelpCommands } from "../domain/interfaces/iHelpCommands";
import { HelpCommands } from "../domain/useCases/helpCommands";
import { IDataManagementRepo } from "../infrastructure/interface/iDataManagementRepo";
import { IDiscordRepo } from "../infrastructure/interface/iDiscordRepo";
import { IKeyVaultRepo } from "../infrastructure/interface/iKeyVaultRepo";
import { DataManagementRepo } from "../infrastructure/repos/dataStorage/dataManagementRepo";
import { DiscordRepo } from "../infrastructure/repos/services/discordRepo";
import { KeyVaultRepo } from "../infrastructure/repos/services/keyVaultRepo";
import { TYPES } from "./types";

const myContainer = new Container();

// Repos DI injections
myContainer.bind<IKeyVaultRepo>(TYPES.IKeyVaultRepo).to(KeyVaultRepo).inSingletonScope();
myContainer.bind<IDiscordRepo>(TYPES.IDiscordRepo).to(DiscordRepo).inSingletonScope();
myContainer.bind<IDataManagementRepo>(TYPES.IDataManagementRepo).to(DataManagementRepo).inSingletonScope();

// Use cases DI injections
myContainer.bind<IHelpCommands>(TYPES.IHelpCommands).to(HelpCommands).inSingletonScope();

export { myContainer };