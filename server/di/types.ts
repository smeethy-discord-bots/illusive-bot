const TYPES = {
    IKeyVaultRepo: Symbol.for("IKeyVaultRepo"),
    IDiscordRepo: Symbol.for("IDiscordRepo"),
    IHelpCommands: Symbol.for("IHelpCommands"),
    IDataManagementRepo: Symbol.for("IDataManagementRepo")
};

export { TYPES };