import { Client, MessageEmbed } from "discord.js";
import { injectable } from "inversify";
import { IHelpCommands } from "../interfaces/iHelpCommands";

@injectable()
export class HelpCommands implements IHelpCommands {
  /**
   * Example use case where the discord user would type !ping
   * @param client discord client instance
   * @param prefix prefix used (! or / ...)
   */
  public DemoCommand(client: Client, prefix: string): void {
    client.on("message", function (message) {
      if (message.author.bot) return;
      if (!message.content.startsWith(prefix)) return;

      const commandBody = message.content.slice(prefix.length);
      const args: string[] = commandBody.split(" ") as string[];
      const command = args.shift()?.toLowerCase();
      const exampleEmbed = new MessageEmbed()
        .setColor("#0099ff")
        .setTitle("Some title")
        .setURL("https://discord.js.org/")
        .setAuthor(
          "Some name",
          "https://i.imgur.com/wSTFkRM.png",
          "https://discord.js.org"
        )
        .setDescription("Some description here")
        .setThumbnail("https://i.imgur.com/wSTFkRM.png")
        .addFields(
          { name: "Regular field title", value: "Some value here" },
          { name: "\u200B", value: "\u200B" },
          {
            name: "Inline field title",
            value: "Some value here",
            inline: true,
          },
          { name: "Inline field title", value: "Some value here", inline: true }
        )
        .addField("Inline field title", "Some value here", true)
        .setImage("https://i.imgur.com/wSTFkRM.png")
        .setTimestamp()
        .setFooter("Some footer text here", "https://i.imgur.com/wSTFkRM.png");

      if (command === "ping") {
        const timeTaken = Date.now() - message.createdTimestamp;
        message.reply(exampleEmbed);
      } else if (command === "sum") {
        const numArgs = args.map((x) => parseFloat(x));
        const sum = numArgs.reduce((counter, x) => (counter += x));
        message.reply(`The sum of all the arguments you provided is ${sum}!`);
      }
    });
  }
}
