import mongoose, { model } from "mongoose";

// Interface also use as type check
export interface ISampleEntity extends mongoose.Document {
  name: string;
  somethingElse?: number;
}

// Mongoose entity
export const SampleEntitySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  somethingElse: Number,
});

// Register and export
const SampleEntity = mongoose.model<ISampleEntity>(
  "SampleEntity",
  SampleEntitySchema
);
export default SampleEntity;
