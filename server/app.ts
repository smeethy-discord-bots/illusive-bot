import express from "express";
import "reflect-metadata";
import { myContainer } from "./di/inversify.config";
import { TYPES } from "./di/types";
import { IHelpCommands } from "./domain/interfaces/iHelpCommands";
import { IDataManagementRepo } from "./infrastructure/interface/iDataManagementRepo";
import { IDiscordRepo } from "./infrastructure/interface/iDiscordRepo";

// rest of the code remains same
const app = express();
const PORT = 8000;
const discordService = myContainer.get<IDiscordRepo>(TYPES.IDiscordRepo);
const helperService = myContainer.get<IHelpCommands>(TYPES.IHelpCommands);
const dataService = myContainer.get<IDataManagementRepo>(
  TYPES.IDataManagementRepo
);

// Get apis
require("./apis");

// Setup
app.listen(PORT, async () => {
  // Init DB
  await dataService.initMongoConnection(process.env.MONGO_BASE_URL as string);

  // Init Discord.js client
  const client = await discordService.initClient();

  // Use Cases - Sample Demo command
  helperService.DemoCommand(client, "!");
  console.log(`[server]: Server is running at https://localhost:${PORT}`);
});

export { app };
