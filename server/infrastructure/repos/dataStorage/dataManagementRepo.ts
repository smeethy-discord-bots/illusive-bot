import { Client } from "discord.js";
import { url } from "inspector";
import { injectable, inject } from "inversify";
import { Db, MongoClient } from "mongodb";
import mongoose from "mongoose";
import { TYPES } from "../../../di/types";
import { IDataManagementRepo } from "../../interface/iDataManagementRepo";
import { IKeyVaultRepo } from "../../interface/iKeyVaultRepo";
import { KeyVaultRepo } from "../services/keyVaultRepo";

@injectable()
export class DataManagementRepo implements IDataManagementRepo {

    private _database:mongoose.Connection;
    private _kvRepo:KeyVaultRepo;
	public constructor(
        @inject(TYPES.IKeyVaultRepo) kvRepo: KeyVaultRepo
	) {
        this._database = new mongoose.Connection();
        this._kvRepo = kvRepo;
	}

    /**
     * Initializes the DB connection
     * @param mongoDbUrl Base mongo url - Internal docker dns
     */
  	public async initMongoConnection(mongoDbUrl:string): Promise<void> {
        let mongoUrl = await this.generateMongoUrl(mongoDbUrl);
        let db = await mongoose.connect(mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true});
        db.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
        this._database = db.connection;
    }
      
    /**
     * Helper fnc
     * @param mongoBaseDBurl Base mongo url - Internal docker dns
     */
    private async generateMongoUrl(mongoBaseDBurl:string):Promise<string> {
        let dbName = await this._kvRepo.getSecret("IllusiveManDiscordDBName");
        let user = await this._kvRepo.getSecret("IllusiveManDiscordAdminUsername");
        let pass = await this._kvRepo.getSecret("IllusiveManDiscordAdminPassword");

        return `mongodb://${mongoBaseDBurl}/${dbName.value}`;
    }

    /**
     * Returns the current connection obj
     */
    public getCurrentConnection():mongoose.Connection {
        return this._database;
    }
}