import { DefaultAzureCredential } from "@azure/identity";
import { KeyVaultSecret, SecretClient } from "@azure/keyvault-secrets";
import { injectable } from "inversify";
import { IKeyVaultRepo } from "../../interface/iKeyVaultRepo";

@injectable()
export class KeyVaultRepo implements IKeyVaultRepo {
  public async getSecret(secretName: string): Promise<KeyVaultSecret> {
    const credential = new DefaultAzureCredential();
    const client = new SecretClient(
      process.env.AZURE_KV_URL as string,
      credential
    );

    const retrievedSecret = await client.getSecret(secretName);

    return retrievedSecret;
  }
}
