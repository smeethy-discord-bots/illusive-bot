import { inject, injectable } from "inversify";
import { IDiscordRepo } from "../../interface/iDiscordRepo";
import { IKeyVaultRepo } from "../../interface/iKeyVaultRepo";
import { TYPES } from "../../../di/types";
import { KeyVaultRepo } from "./keyVaultRepo";
import { Client } from 'discord.js';

@injectable()
export class DiscordRepo implements IDiscordRepo {

	private _kvRepo:IKeyVaultRepo;
	public constructor(
		@inject(TYPES.IKeyVaultRepo) kvRepo: KeyVaultRepo
	) {
		this._kvRepo = kvRepo;
	}

	/**
	 * Gets the token off the key vault and inits discord bot backend
	 */
  	public async initClient(): Promise<Client> {
		let botToken = await this._kvRepo.getSecret("IllusiveManDiscordBotToken");
    	const client = new Client();
		client.login(botToken.value);
		return client;
  	}
}
