import { KeyVaultSecret } from "@azure/keyvault-secrets";

export interface IKeyVaultRepo {
    getSecret(secretName:string): Promise<KeyVaultSecret>
}