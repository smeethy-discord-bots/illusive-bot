import mongoose from "mongoose";

export interface IDataManagementRepo {
    initMongoConnection(mongoDbUrl:string): Promise<void>;
    getCurrentConnection():mongoose.Connection;
}