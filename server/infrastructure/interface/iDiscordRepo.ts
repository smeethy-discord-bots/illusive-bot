import { Client } from "discord.js";

export interface IDiscordRepo {
    initClient(): Promise<Client>;
}