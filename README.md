# Documentation
This sample Discord Bot backend composed by an expressJS server processing the requests and a mongoDB instance, features:
1. TypeScript backbone.
2. Dependency Injection with InversifyJS.
3. Security with an Azure Key vault secret retrieval implementation (unless you plan on hosting on Azure and use a managed ID).
4. MongoDB instance for data persistence.

# Requirements 
1. An Azure App Registration for the bot backend, keep the client id and create a client secret.
2. An Azure Key Vault instance, keep the KV url.
3. Storing required secrets on this later (to be centralized in a future update)
4. Simply launch the docker compose with docker-compose up -d on the host machine.
 