# Env variables to create on the host to retrieve secrets
setx AZURE_CLIENT_ID myAzureAppRegistrationClientId
setx AZURE_TENANT_ID myAzureTenantId
setx AZURE_CLIENT_SECRET myAzureAppRegistrationClientSecret
setx AZURE_KV_URL myAzureKvUrl

# Base mongo url for this docker host internal dns
setx MONGO_BASE_URL dockerComposeMongoContainerName:dockerComposeMongoContainerPort